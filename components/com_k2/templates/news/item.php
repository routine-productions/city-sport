<header>
    <small class="News-Date"><?= JHTML::_('date', $this->item->created, 'd F Y'); ?></small>
    <h1><?= $this->item->title; ?></h1>
</header>
<div class="News-Content">
    <?= $this->item->introtext; ?>
    <?= $this->item->fulltext; ?>
</div>