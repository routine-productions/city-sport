// Remove Empty Days
$('.Schedule-Frame-Wrapper').each(function () {
    var Frame = this;
    var Indexes = [];
    $('.Schedule-Table tr', Frame).each(function () {
        if ($(this).text().trim().length < 2) {
            Indexes.push($(this).index());
            $(this).remove();
        }
    });

    $.each(Indexes, function (Value) {
        $($('.Schedule-Time tr', Frame).get(this)).remove();
    });
});


$('.Slides article').css('height', 'auto');
$('.Slides').height($('.Slides article').outerHeight());
$('.Slides article').css('height', '100%');

$(window).resize(function () {
    $('.Slides article').css('height', 'auto');
    $('.Slides').height($('.Slides article').outerHeight());
    $('.Slides article').css('height', '100%');
});


// Change Table
$($('.Schedule-Frame-Wrapper').get(0)).addClass('Visible');

$('.Link-Script').click(function () {
    if ($(this).text() == 'Взрослых') {
        $(this).text('Детей');
    } else {
        $(this).text('Взрослых');
    }
    $('.Schedule-Frame-Wrapper').toggleClass('Visible');

    return false;
});

// Slider

$('.Membership-Slider').easySlider(
    {
        'Effect': 'slide',
        'Height': 'auto'
    }
);

$('.Gallery-Slider').easySlider(
    {
        'Effect': 'slide',
        'Height': 'none'
    }
);

$('.News-Lins').easySlider(
    {
        'Effect': 'fade',
        'Height': 'auto'
    }
);

$('.Site-Header .Slides').easySlider(
    {
        'Effect': 'fade',
        //'Height': 'auto'
    }
);


$('.News .Ghost-Button').click(function () {
    if ($('.News-Article-Slide:visible:last').next().length) {
        $('.News-Article-Slide:visible:last').next().show().addClass('Able');
    } else {
        $('.News .Ghost-Button').remove();
    }
    return false;
});


// Map Init

function Map_Init() {
    var mapOptions = {
        zoom: 17,
        center: new google.maps.LatLng(55.961707, 37.303548)
    };
    var mapElement = $('#Google-Map')[0];
    var map = new google.maps.Map(mapElement, mapOptions);

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(55.961707, 37.303548),
        map: map,
        title: 'г. Сходня, ул. Микояна д. 10 к.6'
    });
    marker.setIcon('/img/geo.png');
}

Map_Init();


// Parallax
if ($(window).width() > 1024) {
    $(window).scroll(function () {
        var Coords = 'center ' + ( -($(window).scrollTop() + $(window).height() - $('.Photo-Gallery').outerHeight() - $('.Photo-Gallery').offset().top) / 2) + 'px';
        $('.Photo-Gallery').css({backgroundPosition: Coords});
    });

    $(window).scroll(function () {
        var Coords = 'center ' + ( -$(window).scrollTop() / 2) + 'px';
        $('.Slider-Frame').css({backgroundPosition: Coords});
    });

}


// Mask
$('.JS-Form-Mask').mask('0 (000) 000-00-00', {
    onComplete: function (Result, Event) {
        $(Event.currentTarget).addClass('JS-Form-Masked');
    }, onKeyPress: function (Result, Event) {
        $(Event.currentTarget).removeClass('JS-Form-Masked');
    }
});

// Modal Time
$('.Popup').click(function () {
    $('#Modal-Time').addClass('Visible');
    $('.Site-Header,.About-Gym-Wrapper,.Photo-Gallery,.News,.Our-Team,.Membership,.Schedule,.Site-Footer').addClass('Blur');
    var Start_Time = $($(this).parents('.Schedule-Frame-Wrapper').find('.Schedule-Time tr').get($(this).parents('tr').index())).text();


    var Split_Time = Start_Time.split(':');
    var Split_Duration = $(this).attr('data-time').split('.');
    var End_Time = Start_Time;
    if (Split_Duration[1]) {
        if (Split_Duration[1] > 10) {
            Split_Duration[1] = Split_Duration[1] / 10;
        }
        var Minutes = (parseInt(Split_Time[1]) + (parseFloat(Split_Duration[1]) * 6));
        if (Minutes == 60) {
            Minutes = '00';
            Split_Duration[0]++;
        }
        End_Time = (parseInt(Split_Time[0]) + parseInt(Split_Duration[0])) + ':' + Minutes;
    } else {
        End_Time = (parseInt(Split_Time[0]) + parseInt(Split_Duration[0])) + ':00';
    }


    $('#Modal-Time .Time').text(Start_Time + ' - ' + End_Time);
    $('#Modal-Time .Title').text($(this).find('i').text());
    $('#Modal-Time .Description').text($(this).attr('data-description'));
    $('#Modal-Time .Trainer span').text($(this).find('small').text());
    $('#Modal-Time .Duration span').text($(this).attr('data-time') + " ч.");
});


function OuterHeight() {
    var Heights = $('.Outer-Slides .Slider-Frame').map(function () {
        return $(this).outerHeight();
    }).get();
    var Max_Height = Math.max.apply(null, Heights);
    $('.Outer-Slides').height(Max_Height);
}


$(window).load(function () {
    OuterHeight();
});
$(window).resize(function () {
    OuterHeight();
});


// Map
$(window).load(function () {
    $('.Schedule-Frame').mCustomScrollbar({
        mouseWheel: {
            enable: false,
            preventDefault: false,
        },
        axis: "x"
    });
});







