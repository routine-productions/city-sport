<?php defined('_JEXEC') or die; ?>

<div class="Membership-List">
    <?php foreach ($items as $key => $item) { ?>
        <article class="Membership-Slide Section-Slider-Item<?= ($key == 0) ? ' Active' : '' ?>">
            <div class="Membership-Card">
                <img src="<?= $item->image; ?>" alt="">
            </div>
            <div class="Membership-Info">
                <h4><?= $item->title; ?></h4>
                <?= $item->introtext; ?>
            </div>
        </article>
    <?php } ?>
</div>

<a class="Arrow Left Section-Slider-Previous">
    <svg>
        <use xlink:href="img/sprite.svg#arrow"></use>
    </svg>
</a>
<a class="Arrow Right Section-Slider-Next">
    <svg>
        <use xlink:href="img/sprite.svg#arrow"></use>
    </svg>
</a>


<ul class="Slider-Pagination">
    <?php foreach ($items as $key => $item) { ?>
        <li class="Section-Slider-Button<?= ($key == 0) ? ' Active' : '' ?>"><a></a></li>
    <?php } ?>
</ul>

