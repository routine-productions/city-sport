<?php defined('_JEXEC') or die;
$Group = 0;
?>
<div class="News-Wrapper">
    <?php foreach ($items as $key => $item) { ?>

    <?php if ($key > 0 && $key % 3 == 0) { ?>
</div>
<?php } ?>
<?php if ($key % 3 == 0) {
$Group++; ?>

<div class="News-Article-Slide Section-Slider-Item<?= ($key == 0) ? ' Active' : '' ?>">
    <?php } ?>

    <article class="News-Article">
        <small class="News-Date"><?= JHTML::_('date', $item->created, 'd F Y'); ?></small>
        <a class="News-Title" href="<?= $item->link; ?>"><?= $item->title; ?></a>
        <div class="News-Content">
            <?= $item->introtext; ?>
        </div>
    </article>

    <?php } ?>
</div>

</div>


<ul class="Slider-Pagination">
    <?php
    for ($Index = 0; $Index < $Group; $Index++) {
        ?>
        <li class="Section-Slider-Button<?= ($Index == 0) ? ' Active' : '' ?>"><a></a></li>
        <?php
    }
    ?>
</ul>

<span class="Section-Slider-Previous">
</span>
<span class="Section-Slider-Next">
</span>
