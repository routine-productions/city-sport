<?php defined('_JEXEC') or die; ?>
<div class="Gallery-Slider">
    <div class="Gallery-Wrapper">
        <?php foreach ($items as $key => $item) {
//            $image = str_replace("cache", "src", $item->image);
//            $image = str_replace(array("_XS", "_S", "_M", "_L", "_XL"), "", $image);

            $image = '/media/k2/items/src/'.md5("Image".$item->id).'.jpg';
            ?>
            <div class="JS-Image-Align Gallery-Item Section-Slider-Item<?= ($key == 0) ? ' Active' : '' ?>" style="height: 100%">
                <img src="<?= $image; ?>"
                     alt="">
            </div>

        <?php } ?>
    </div>
    <div class="Arrow Left Gallery-Next Section-Slider-Previous">
        <svg>
            <use xlink:href="img/sprite.svg#arrow"></use>
        </svg>
    </div>
    <div class="Arrow Right Gallery-Prev Section-Slider-Next">
        <svg>
            <use xlink:href="img/sprite.svg#arrow"></use>
        </svg>
    </div>
</div>