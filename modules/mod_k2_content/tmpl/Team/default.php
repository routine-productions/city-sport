<?php defined('_JEXEC') or die; ?>
<?php foreach ($items as $key => $item) { ?>
    <li>
        <a>
            <header>
                <h5 class="Trainer-Name"><?= $item->title; ?></h5>
                <div class="Trainer-Position"><?= $item->introtext; ?></div>
            </header>
            <div class="Back"><img src="<?= $item->image; ?>" alt=""></div>
        </a>
    </li>
<?php } ?>