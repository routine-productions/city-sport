<?php
$Table_Rows = [];
$Time = [
    '10:00', '10:30', '11:00', '11:30', '12:00', '12:30',
    '13:00', '13:30', '14:00', '14:30', '15:00', '15:30',
    '16:00', '16:30', '17:00', '17:30', '18:00', '18:30',
    '19:00', '19:30', '20:00', '20:30', '21:00', '21:30',
    '22:00', '22:30'
];

date_default_timezone_set('Europe/Moscow');

foreach ($items as $key => $item) {
    $Table_Rows['header'][$key]['day'] = $item->title;

    for ($Row = 0; $Row < 26; $Row++) {

        $Table_Rows[$Row][$key] = [
            'info' => isset($item->extra_fields[$Row * 2]->value) ? $item->extra_fields[$Row * 2]->value : '',
            'description' => isset($item->extra_fields[$Row * 2 + 1]->value) ? $item->extra_fields[$Row * 2 + 1]->value : ''
        ];
    }
}
?>
<div class="Schedule-Frame-Wrapper">
    <table class="Schedule-Time">
        <tr>
            <th></th>
        </tr>
        <?php foreach ($Time as $Time_Item): ?>
            <tr>
                <td><?= $Time_Item ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <div class="Schedule-Frame">
        <table class="Schedule-Table">
            <?php foreach ($Table_Rows as $Row_Key => $Row) { ?>
                <tr>
                    <?php foreach ($Row as $Key_Training => $Training) {
                        // Header Days
                        if ($Row_Key === 'header') { ?>
                            <th><?= $Training['day'] ?></th>
                        <?php } else {
                            $Training_Info_Double = explode('|', $Training['info']);

                            // Ein Training
                            if (empty($Training_Info_Double[1])) { ?>
                                <?php
                                $Training_Info = explode(',', $Training['info']);
                                if (empty($Training_Info[1])) {
                                    $Training_Info[1] = '';
                                }
                                if (empty($Training_Info[2])) {
                                    $Training_Info[2] = 0.5;
                                }
                                if (!empty($Training_Info[0])) {
                                    ?>
                                    <td class='Popup' data-time='<?= trim($Training_Info[2]) ?>'
                                        data-description='<?= trim($Training['description']) ?>'>
                                        <i><?= trim($Training_Info[0]) ?></i> <em>(<?= trim($Training_Info[2]) ?>
                                            ч.)</em>
                                        <small><?= trim($Training_Info[1]) ?></small>
                                    </td>
                                <?php } else { ?>
                                    <td class="Odd"></td>
                                <?php } ?>
                            <?php } else {
                                // Double Training

                                $Training_Info_Double[0] = explode(',', $Training_Info_Double[0]);
                                $Training_Info_Double[1] = explode(',', $Training_Info_Double[1]);
                                $Training_Description = explode('|', $Training['description']);

                                if (empty($Training_Description[0])) {
                                    $Training_Info[0] = '';
                                }
                                if (empty($Training_Description[1])) {
                                    $Training_Info[1] = '';
                                }

                                if (empty($Training_Info_Double[0][1])) {
                                    $Training_Info_Double[0][1] = '';
                                }
                                if (empty($Training_Info[2])) {
                                    $Training_Info_Double[0][2] = 0.5;
                                }
                                if (empty($Training_Info_Double[1][1])) {
                                    $Training_Info_Double[1][1] = '';
                                }
                                if (empty($Training_Info_Double[1][2])) {
                                    $Training_Info_Double[1][2] = 0.5;
                                }
                                ?>

                                <td class="Double">
                                <span class='Popup'
                                      data-time='<?= trim($Training_Info_Double[0][2]) ?>'
                                      data-description='<?= trim($Training_Description[0]) ?>'>
                                    <i><?= trim($Training_Info_Double[0][0]) ?></i>
                                    <em>(<?= trim($Training_Info_Double[0][2]) ?> ч.)</em>
                                <small><?= trim($Training_Info_Double[0][1]) ?></small></span>
                                  <span class='Popup'
                                        data-time='<?= trim($Training_Info_Double[1][2]) ?>'
                                        data-description='<?= trim($Training_Description[1]) ?>'>
                                      <i><?= trim($Training_Info_Double[1][0]) ?></i>
                                      <em>(<?= trim($Training_Info_Double[1][2]) ?> ч.)</em>
                                <small><?= trim($Training_Info_Double[1][1]) ?></small></span>
                                </td>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>