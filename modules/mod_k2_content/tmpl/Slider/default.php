<?php defined('_JEXEC') or die; ?>
<div class="Outer-Slides">
    <?php foreach ($items as $key => $item) { ?>
        <?php
        $title = explode('|', $item->title);

        $image = str_replace("cache", "src", $item->image);
        $image = str_replace(array("_XS", "_S", "_M", "_L", "_XL"), "", $image);
        ?>

        <div class="Slider-Frame Section-Slider-Item<?= ($key == 0) ? ' Active' : '' ?>"
             style="background-image: url(<?= $image; ?>)">
            <div class="Slider-Inner JS-Vertical-Align">
                <h3><?= $title[0] ?>
                    <?php if (isset($title[1])) { ?>
                        <small><?= $title[1] ?></small>
                    <?php } ?>
                </h3>
                <?= $item->introtext; ?>
            </div>
        </div>
    <?php } ?>
</div>

<?php if (count($items) > 1) { ?>
    <ul class="Slider-Pagination">
        <?php foreach ($items as $key => $item) { ?>
            <li class="Section-Slider-Button<?= ($key == 0) ? ' Active' : '' ?>"><a></a></li>
        <?php } ?>
    </ul>

    <a class="Arrow Left Section-Slider-Previous">
        <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#arrow"></use>
        </svg>
    </a>
    <a class="Arrow Right Section-Slider-Next">
        <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#arrow"></use>
        </svg>
    </a>

<?php } ?>

<a href="#about" class="Scroll-Down JS-Scroll-Button">
    <svg>
        <use xlink:href="img/sprite.svg#arrow"></use>
    </svg>
</a>

