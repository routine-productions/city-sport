<?php defined('_JEXEC') or die('Restricted access');
$URI = array_values(array_filter(explode('?', $_SERVER['REQUEST_URI'])));
$URI = array_values(array_filter(explode('/', $URI[0])));
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>City Sport</title>
    <link rel="stylesheet" href="css/index.min.css">
    <link rel="apple-touch-icon" sizes="57x57" href="img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="img/fav/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="img/fav/manifest.json">
    <link rel="mask-icon" href="img/fav/safari-pinned-tab.svg" color="#3a6eb4">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name = "format-detection" content = "telephone=no">
</head>
<body>

<main>
    <?php
    if (empty($URI[0])) {
        require_once __DIR__ . '/modules/header.php';
    } else {
        require_once __DIR__ . '/modules/header-inner.php';
    }
    ?>


    <?php
    if (empty($URI[0])) {
        require_once __DIR__ . '/modules/home.php';
    } else {
        require_once __DIR__ . '/modules/' . $URI[0] . '.php';
    }
    ?>
</main>




<?php
if (empty($URI[0])) {
    require_once __DIR__ . '/modules/footer.php';
} else {
    require_once __DIR__ . '/modules/footer-inner.php';
}
?>

<script src="/index.min.js"></script>

</body>
</html>