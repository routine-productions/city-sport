<!-- START: Site Footer -->
<footer class="Site-Footer" id="contacts">
    <div class="Contacts">
        <h3>Контакты</h3>
        <div class="Company-Phone">
            <svg>
                <use xlink:href="img/sprite.svg#phone"></use>
            </svg>
				<span>
                    <jdoc:include type="modules" name="Phone"/>
                    <jdoc:include type="modules" name="Worktime"/>
				</span>
        </div>
        <div class="Company-Address">
            <svg>
                <use xlink:href="img/sprite.svg#earth"></use>
            </svg>
				<span>
					 <jdoc:include type="modules" name="Address"/>
				</span>
        </div>
    </div>

    <div class="Map JS-Map-Descroll">
<!--        <jdoc:include type="modules" name="Map"/>-->
        <div id="Google-Map"></div>
    </div>

    <div class="Bottom">
        <div class="Footer-Inner">
            <ul class="Social-Icons">
                <li>
                    <a href="http://instagram.com/city__sport">
                        <svg>
                            <use xlink:href="img/sprite.svg#instagram"></use>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="http://vk.com/city.sport">
                        <svg>
                            <use xlink:href="img/sprite.svg#vkontakte"></use>
                        </svg>
                    </a>
                </li>
<!--                <li>-->
<!--                    <a href="#">-->
<!--                        <svg>-->
<!--                            <use xlink:href="img/sprite.svg#facebook"></use>-->
<!--                        </svg>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="#">-->
<!--                        <svg>-->
<!--                            <use xlink:href="img/sprite.svg#youtube"></use>-->
<!--                        </svg>-->
<!--                    </a>-->
<!--                </li>-->
            </ul>
            <div class="Company-Phone">
                <jdoc:include type="modules" name="Phone"/>
                <jdoc:include type="modules" name="Worktime"/>
            </div>

            <a href="#" class="Logotype JS-Scroll-Top">
                <svg>
                    <use xlink:href="img/sprite.svg#logo"></use>
                </svg>
            </a>
        </div>
    </div>
</footer>