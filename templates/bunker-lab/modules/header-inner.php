<!-- START: HEADER0-INNER-->
<header class="Site-Header Inner">
    <div class="Top-Bar">
        <a class="Logotype" href="/">
            <img src="img/logo.png" srcset="img/logo@2x.png 2x" alt="">
            <span>Семейный фитнес&ndash;центр</span>
        </a>
        <nav>
            <ul>
                <li><a href="/#about">о клубе</a></li>
                <li><a href="/#membership">Членство</a></li>
                <li><a href="/#timetable">Расписание</a></li>
                <li><a href="/#contacts">Контакты</a></li>
            </ul>
        </nav>
        <div class="Company-Phone">
            <jdoc:include type="modules" name="Phone"/>
            <jdoc:include type="modules" name="Worktime"/>
        </div>
    </div>
</header>
