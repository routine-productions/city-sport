<!-- ABOUT -->
<div class="About-Gym-Wrapper" id="about">
    <section class="About-Gym">
        <h3>О клубе</h3>
        <jdoc:include type="modules" name="About"/>

        <h4>Фитнес&ndash;клуб вмещает</h4>
        <div class="Advantages">
            <jdoc:include type="modules" name="Advantages"/>
        </div>
    </section>
</div>


<!-- START:  -->
<section class="Photo-Gallery">
    <div class="JS-Vertical-Align">
        <h3>Фотогалерея</h3>
        <a class="Ghost-Button JS-Modal-Button" data-modal-id="Modal-Slider">Смотреть фото</a>
    </div>
</section>

<!-- MODAL -->
<div class="Modal JS-Modal Modal-Slider" id='Modal-Slider'>
    <div class="Blur"></div>
    <div class="Modal-Inner Modal-Slider-Inner JS-Modal-Box">
        <a class="Close JS-Modal-Close">
            <svg>
                <use xlink:href="img/sprite.svg#close"></use>
            </svg>
        </a>
        <jdoc:include type="modules" name="Gallery"/>
    </div>
</div>

<!-- START: News Slider -->
<div class="News">

    <div class="News-Lins">
        <jdoc:include type="modules" name="News"/>
    </div>

    <a class="Ghost-Button" href="#">Показать ещё</a>

</div>

<!-- START:  -->
<ul class="Our-Team">
    <jdoc:include type="modules" name="Team"/>
</ul>

<!-- START:  -->
<div class="Membership" id="membership">
    <section class="Membership-Inner">
        <h3>Членство</h3>
        <div class="Membership-Slider">


            <jdoc:include type="modules" name="Membership"/>

        </div>
    </section>
    <a class="Ghost-Button JS-Modal-Button" data-modal-id="Modal-100">Вступить в клуб</a>
</div>

<!-- MODAL -->
<div class="Modal JS-Modal" id='Modal-100'>
    <div class="Modal-Inner Modal-White JS-Modal-Box">
        <a class="Close JS-Modal-Close">
            <svg>
                <use xlink:href="img/sprite.svg#close"></use>
            </svg>
        </a>
        <h5>Членство в клубе</h5>
        <p>Пожалуйста, заполните анкету и мы Вам перезвоним в ближайшее время</p>

        <form class='JS-Form'
              data-form-email='citysport@list.ru'
              data-form-subject='CITY-SPORT: Членство в клубе'
              data-form-url='/js/data.form.php'
              data-form-method='POST'>
            <div class="Form-Row"><label for="firstName">Имя</label><input class="JS-Form-Require" id="firstName"
                                                                           type="text" name='first-name'
                                                                           data-input-title='Имя'></div>
            <div class="Form-Row"><label for="lastName">Фамилия</label><input class="JS-Form-Require" id=lastName"
                                                                              type="text" name='last-name'
                                                                              data-input-title='Фамилия'></div>
            <div class="Form-Row"><label for="email">Эл. почта</label><input class="JS-Form-Require JS-Form-Email"
                                                                             id="email" type="text" name='email'
                                                                             data-input-title='Эл. почта'></div>
            <div class="Form-Row"><label for="phone">Телефон</label><input class="JS-Form-Require JS-Form-Mask"
                                                                           id="phone" type="text" name='phone'
                                                                           data-input-title='Телефон'></div>
            <div class="Form-Row Checkbox"><input id="subscribe" type="checkbox" name='subscribe' data-input-title='Подписка на
                    новости клуба' value="Да"><label for="subscribe">Подписаться на
                    новости клуба</label></div>
            <button class="Submit JS-Form-Button">Отправить</button>
            <div class="JS-Form-Result"></div>
        </form>
    </div>
</div>

<!-- MODAL -->
<div class="Modal JS-Modal" id='Modal-Time'>
    <div class="Modal-Inner Training-Modal-Outer JS-Modal-Box JS-Vertical-Align">
        <a class="Close JS-Modal-Close">
            <svg>
                <use xlink:href="img/sprite.svg#close"></use>
            </svg>
        </a>
        <div class="Training-Modal">
            <h6 class="Time"></h6>
            <h4 class="Title"></h4>
            <p class="Description"></p>

            <div class="Trainer">
                <svg>
                    <use xlink:href="img/sprite.svg#trainer"></use>
                </svg>
                <div>
                    <small>Тренер</small>
                    <span></span>
                </div>
            </div>

            <div class="Duration">
                <svg>
                    <use xlink:href="img/sprite.svg#duration"></use>
                </svg>
                <div>
                    <small>Длительность</small>
                    <span></span>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- START: Schedule -->
<div class="Schedule" id="timetable">
    <div class="Schedule-Inner">
        <h3>Расписание
            <small>для</small>
            <a href="" class="Link-Script">Взрослых</a>
        </h3>
        <jdoc:include type="modules" name="Timetable"/>
    </div>
</div>
