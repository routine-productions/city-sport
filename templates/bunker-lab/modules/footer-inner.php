<!-- START: Site Footer Inner -->
<footer class="Site-Footer">
    <div class="Bottom">
        <div class="Footer-Inner">
            <ul class="Social-Icons">
                <li>
                    <a href="http://instagram.com/city__sport">
                        <svg>
                            <use xlink:href="/img/sprite.svg#instagram"></use>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="http://vk.com/city.sport">
                        <svg>
                            <use xlink:href="/img/sprite.svg#vkontakte"></use>
                        </svg>
                    </a>
                </li>
                <!--                <li>-->
                <!--                    <a href="#">-->
                <!--                        <svg>-->
                <!--                            <use xlink:href="/img/sprite.svg#facebook"></use>-->
                <!--                        </svg>-->
                <!--                    </a>-->
                <!--                </li>-->
                <!--                <li>-->
                <!--                    <a href="#">-->
                <!--                        <svg>-->
                <!--                            <use xlink:href="/img/sprite.svg#youtube"></use>-->
                <!--                        </svg>-->
                <!--                    </a>-->
                <!--                </li>-->
            </ul>
            <div class="Company-Phone">
                <jdoc:include type="modules" name="Phone"/>
                <jdoc:include type="modules" name="Worktime"/>
            </div>

            <a href="#" class="Logotype">
                <svg>
                    <use xlink:href="/img/sprite.svg#logo"></use>
                </svg>
            </a>
        </div>
    </div>
</footer>