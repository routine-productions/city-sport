<!-- START: HEADER -->
<header class="Site-Header">
    <div class="Top-Bar JS-Mobile-Menu">
        <a class="Logotype" href="#">
            <img src="img/logo.png" srcset="img/logo@2x.png 2x" alt="">
            <span>Семейный фитнес&ndash;центр</span>
        </a>
        <a href="#" class="Hamburger JS-Mobile-Menu-Toggle"><span></span></a>
        <nav class="JS-Mobile-Menu-List">
            <ul class="JS-Page-Navigation">
                <li><a href="#about">о клубе</a></li>
                <li><a href="#membership">Членство</a></li>
                <li><a href="#timetable">Расписание</a></li>
                <li><a href="#contacts">Контакты</a></li>
            </ul>
        </nav>
        <div class="Company-Phone">
            <jdoc:include type="modules" name="Phone"/>
            <jdoc:include type="modules" name="Worktime"/>
        </div>
    </div>
    <div class="Slides">
        <jdoc:include type="modules" name="Slider"/>
    </div>
</header>
